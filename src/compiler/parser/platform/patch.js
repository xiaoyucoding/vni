import {
  createVniPlatformMark,
  hasVniIncludePlatformTag,
  hasVniExcludePlatformTag,
  createUniPlatformIncludeTag,
  createUniPlatformExcludeTag,
  createUniPlatformEndTag,
} from './util'
import { INCLUDE_TAG, EXCLUDE_TAG } from '@/shared'
// 跨平台的配置
export function patchVniPagePlatform(page, options) {
  const platform = createVniPlatformMark(options)
  return {
    ...page,
    ...platform,
  }
}
// uni-app 的条件编译
export function patchUniPagePlatform(vniPage) {
  if (hasVniIncludePlatformTag(vniPage)) {
    const includeValue = vniPage[INCLUDE_TAG]
    delete vniPage[INCLUDE_TAG]
    return [createUniPlatformIncludeTag(includeValue), vniPage, createUniPlatformEndTag()]
  } else if (hasVniExcludePlatformTag(vniPage)) {
    const excludeValue = vniPage[EXCLUDE_TAG]
    delete vniPage[EXCLUDE_TAG]
    return [createUniPlatformExcludeTag(excludeValue), vniPage, createUniPlatformEndTag()]
  } else {
    return null
  }
}

export function patchUniPlatformToPageList(pageList) {
  let list = []
  pageList.forEach((page) => {
    const platforms = patchUniPagePlatform(page)
    if (platforms) {
      list = list.concat(platforms)
    } else {
      list.push(page)
    }
  })
  return list
}
