import { has, isArray } from 'lodash'

import { INCLUDE_TAG, EXCLUDE_TAG, END_TAG, VNI_PLATFORM_TAG, SPLIT_SIGN } from '@/shared'

// 条件编译占位项
export function isUniPlatformStart(data) {
  return isUniIncludeStart(data) || isUniExcludeStart(data)
}
export function isUniIncludeStart(data) {
  return has(data, INCLUDE_TAG)
}
export function isUniExcludeStart(data) {
  return has(data, EXCLUDE_TAG)
}
export function isUniPlatformEnd(data) {
  return has(data, END_TAG)
}
// #start vni
export function hasVniIncludePlatformTag(data) {
  return has(data, INCLUDE_TAG)
}
export function hasVniExcludePlatformTag(data) {
  return has(data, EXCLUDE_TAG)
}
// #end
// 生成 vni 规范的标记
export function createVniPlatformMark(data) {
  const platform = {}
  if (isUniIncludeStart(data)) {
    platform[INCLUDE_TAG] = translateUniPlatformToVni(data[INCLUDE_TAG])
  } else if (isUniExcludeStart(data)) {
    platform[EXCLUDE_TAG] = translateUniPlatformToVni(data[EXCLUDE_TAG])
  }
  return platform
}

export function translateUniPlatformToVni(value) {
  return value.split(SPLIT_SIGN)
}

export function translateVniPlatformToUni(value) {
  // vni -> uni 全部大写
  const platformText = isArray(value) ? value.join(` ${SPLIT_SIGN} `) : value
  return platformText.toUpperCase()
}

export function createUniPlatformIncludeTag(values) {
  return {
    [VNI_PLATFORM_TAG]: `// #ifdef ${translateVniPlatformToUni(values)}`,
  }
}
export function createUniPlatformExcludeTag(values) {
  return {
    [VNI_PLATFORM_TAG]: `// #ifndef ${translateVniPlatformToUni(values)}`,
  }
}
export function createUniPlatformEndTag() {
  return {
    [VNI_PLATFORM_TAG]: `// #endif`,
  }
}
