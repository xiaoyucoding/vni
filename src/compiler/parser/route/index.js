import { patchPageList, groupPageList } from './pages'
import { groupSubpackagePageList } from './subpackages'
import { cloneObjectByFilterKeys, HOME_PAGE_TAG } from '@/shared'
import { findTabBarPageIndex, patchVniTabBarPage } from '../tab-bar'
import { isEmpty } from 'lodash'

// 将 uni 规范的路由配置转换为 vni 规范
export function parseUniRouteToVni(pagesJsonObject) {
  const { pages, subpackages, tabBar } = pagesJsonObject
  let routes = []
  // 首页 应该不会有第一个就是条件编译的情况吧？
  pages[0][HOME_PAGE_TAG] = true
  // pages
  routes = routes.concat(patchPageList(pages))
  // subpackages
  if (subpackages) {
    subpackages.forEach((subpackage) => {
      const subpackageOptions = cloneObjectByFilterKeys(subpackage, ['pages'])
      const subpackageRoutes = patchPageList(subpackage.pages, subpackageOptions)
      routes = routes.concat(subpackageRoutes)
    })
  }
  if (tabBar && tabBar.list) {
    tabBar.list.forEach((tab) => {
      const tabIndex = findTabBarPageIndex(routes, tab)
      if (tabIndex > -1) {
        routes[tabIndex] = patchVniTabBarPage(routes[tabIndex], tab)
      }
    })
  }
  return routes
}

// 转换为 uni-app 规范的路由配置
export function parserVniRouteToUni(routes) {
  const pageGroup = groupPageList(routes)
  // 分包
  const subpackagePageList = pageGroup.subpackagePageList
  let subpackages = []
  if (!isEmpty(subpackagePageList)) {
    // 处理分包
    subpackages = groupSubpackagePageList(subpackagePageList)
  }
  return {
    pages: {
      pages: pageGroup.mainPageList,
      subpackages,
    },
    tabBarPageList: pageGroup.tabBarPageList,
  }
}
