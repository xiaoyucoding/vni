import { parseUniPlatformToVni, parseVniPlatformToUni } from './platform'
import { parseUniRouteToVni, parserVniRouteToUni } from './route'
import { parserUniTabBarToVni } from './tab-bar'
import { parseUniExtrasConfigToVni } from './extras'

// 将 uni-app 的 pages.json 转换成 vni 规范的数据
export function parseUniPagesJsonToVni(pagesJsonString) {
  // 这一步将 pages.json 的数据转换成标准的 JSON 格式
  pagesJsonString = parseUniPlatformToVni(pagesJsonString)
  // 转换为 JS 对象
  const pagesJsonObject = JSON.parse(pagesJsonString)

  const routes = parseUniRouteToVni(pagesJsonObject)

  const tabBar = parserUniTabBarToVni(pagesJsonObject.tabBar)

  const globalStyle = pagesJsonObject.globalStyle

  const easycom = pagesJsonObject.easycom

  const condition = pagesJsonObject.condition

  const extras = parseUniExtrasConfigToVni(pagesJsonObject)

  return {
    routes,
    tabBar,
    globalStyle,
    easycom,
    condition,
    extras,
  }
}

export function parseVniConfigToUniPagesJsonString(vniConfig) {
  const { routes, profile } = vniConfig
  const uniPages = parserVniRouteToUni(routes)
  // 补全 tabBar
  if (profile.tabBar) {
    profile.tabBar.list = uniPages.tabBarPageList
  }
  const pagesJsonObject = {
    ...uniPages.pages,
    ...profile,
  }
  let pagesJsonString = JSON.stringify(pagesJsonObject, null, 4)
  pagesJsonString = parseVniPlatformToUni(pagesJsonString)
  // 处理条件编译
  return pagesJsonString
}
