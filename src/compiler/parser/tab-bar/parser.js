import { cloneObjectByFilterKeys, TAB_BAR_PAGE_TAG } from '@/shared'
// 解析成 vni 规范的 tabBar 配置
export function parserUniTabBarToVni(tabBar) {
  return tabBar ? cloneObjectByFilterKeys(tabBar, ['list']) : null
}
// 选项卡页面打标
export function patchVniTabBarPage(page, options) {
  const tabBarOptions = cloneObjectByFilterKeys(options, ['pagePath'])
  return {
    ...page,
    [TAB_BAR_PAGE_TAG]: tabBarOptions,
  }
}

export function parseVniTabBarPageToUni(page) {
  const pagePath = page.path
  const tabBarOptions = page[TAB_BAR_PAGE_TAG]
  return {
    pagePath,
    ...tabBarOptions,
  }
}
