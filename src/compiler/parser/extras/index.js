import { cloneObjectByFilterKeys } from '@/shared'
// 核心配置项
export const PRIMARY_OPTIONS = ['pages', 'tabBar', 'subPackages', 'subpackages', 'globalStyle', 'easycom', 'condition']
// 忽略掉核心配置之外的所有配置
export function parseUniExtrasConfigToVni(pagesJsonObject) {
  return cloneObjectByFilterKeys(pagesJsonObject, PRIMARY_OPTIONS)
}
