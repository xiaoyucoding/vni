module.exports = {
  base: '/vni/',
  title: 'vni',
  description: 'A uni-app Enhanced Package.',
  host: 'localhost',
  temp: './docs/.temp',
  themeConfig: {
    sidebarDepth: 2,
    displayAllHeaders: true,
    nav: [{
      text: '指南',
      link: '/guide/'
    }, {
      text: '配置',
      link: '/config/'
    }, {
      text: '仓库',
      link: 'https://gitee.com/xiaoyucoding/vni'
    }],
    sidebar: {
      '/guide/': [{
        title: '指南',
        collapsable: false,
        children: ['', 'getting-started', 'command', 'directory-structure']
      }],
      '/config/': [{
        title: '配置',
        collapsable: false,
        children: ['', 'extras']
      }]
    }
  }
}
