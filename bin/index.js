#!/usr/bin/env node

const program = require('commander')
const inquirer = require('inquirer')
const vni = require('../dist')
const pkg = require('../package.json')

program.command('init').action(() => {
  vni.init()
})

program.command('build').action(() => {
  vni.build()
})

program.command('dev').action(() => {
  vni.dev()
})

program.command('clear').action(() => {
  vni.clear()
})
// 新建页面
program
  .command('create')
  .description('新增页面')
  .action(function () {
    inquirer
      .prompt([
        {
          type: 'input',
          name: 'path',
          message: '请输入页面路径',
          validate: function (input) {
            return typeof input === 'string' ? true : '请输入有效的页面路径'
          },
        },
      ])
      .then((answers) => {
        vni.createPage(answers)
      })
      .catch((error) => {
        throw error
      })
  })

// 这段代码始终放最后
program.version(pkg.version, '-v, --version').parse(process.argv)
