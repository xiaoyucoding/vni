import { formatDateTime } from './util'

const colors = require('colors/safe')

function getTagText() {
  return `[${formatDateTime()}]`
}

function getPrefix() {
  return `[vni]${getTagText()}`
}

export function logSuccess(msg) {
  console.log(colors.brightGreen(getPrefix() + msg))
}

export function logInfo(msg) {
  console.log(colors.brightBlue(getPrefix() + msg))
}

export function logWarn(msg) {
  console.log(colors.brightYellow(getPrefix() + msg))
}

export function logError(msg) {
  console.log(colors.brightRed(getPrefix() + msg))
}
