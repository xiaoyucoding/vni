import { outputJsonSync } from 'fs-extra'
import { resolveFilePath, VNI_CONFIG_PATH, vniConfigFileEnum, VNI_PAGE_SUFFIX } from '@/shared'
// 生成 vni 规范的文件
export function writeVniFiles(vniData) {
  const { routes, tabBar, globalStyle, easycom, condition, extras } = vniData
  // 生成路由配置文件
  routes.forEach((route) => {
    outputVniJsonFile(resolveFilePath(`${route.path}${VNI_PAGE_SUFFIX}`), route)
  })
  // tab-bar.json
  if (tabBar) {
    outputVniJsonFile(resolveFilePath(VNI_CONFIG_PATH, vniConfigFileEnum.TAB_BAR), tabBar)
  }
  // global-style.json
  if (globalStyle) {
    outputVniJsonFile(resolveFilePath(VNI_CONFIG_PATH, vniConfigFileEnum.GLOBAL_STYLE), globalStyle)
  }
  // easycom.json
  if (easycom) {
    outputVniJsonFile(resolveFilePath(VNI_CONFIG_PATH, vniConfigFileEnum.EASY_COM), easycom)
  }
  // condition.json
  if (condition) {
    outputVniJsonFile(resolveFilePath(VNI_CONFIG_PATH, vniConfigFileEnum.CONDITION), condition)
  }
  // extras.json
  if (extras) {
    outputVniJsonFile(resolveFilePath(VNI_CONFIG_PATH, vniConfigFileEnum.EXTRAS), extras)
  }
}

export function outputVniJsonFile(path, data) {
  outputJsonSync(`${path}.json`, data, {
    spaces: 4,
  })
}
