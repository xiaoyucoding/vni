# 安装
通过 `npm/yarn` 安装 vni，请确保你的电脑有这些应用程序之一。

::: tip
- vue-cli 3 默认的包管理模式是 yarn，修改方式请在使用搜索引擎搜索 `vue-cli 3 packagemanager` 关键字。
:::

## 全局安装
如果你想在多个项目中使用 vni，你可以全局安装它：

``` bash
# 安装
$ npm install -g vni # 或者 yarn global add vni

# 以下命令需要在 uni-app 项目根目录下执行

# 初始化
$ vni init
```

## 局部安装

如果你想在一个现有项目中使用 vni，也可以选择将 vni 作为项目依赖安装来使用它：

``` bash
# 将 vni 作为一个本地依赖安装
$ npm install --save-dev vni # 或者 yarn add --save-dev vni

# 初始化
$ npx vni init
```

建议在 `package.json` 里加一些命令：

``` json
{
  "scripts": {
    "vni:init": "vni init",
    "vni:build": "vni build",
    "vni:watch": "vni watch"
  }
}
```

然后就可以更方便地使用了：

``` bash
$ npm run vni:init # 或者：yarn vni:init
```
