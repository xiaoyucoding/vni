# 目录结构

## HBuilderX

如果是 HBuilderX 创建的项目，其目录结构如下：

```
├── _vni
│  ├── condition.json
│  ├── easycom.json
│  ├── extras.json
│  ├── global-style.json
│  ├── tab-bar.json
│  └── theme.json
│
├── pages
│  ├── pages/**/*.json
├── platforms
│  ├── platforms/**/*.json
├── manifest.json
├── package.json
└── pages.json
```

## vue-cli

如果是 vue-cli 创建的项目，其目录结构如下：

```
├── src
│  ├── _vni
│  │  ├── condition.json
│  │  ├── easycom.json
│  │  ├── extras.json
│  │  ├── global-style.json
│  │  ├── tab-bar.json
│  │  └── theme.json
│  │
│  ├── pages
│  │  ├── pages/**/*.vni.json
│  ├── platforms
│  │  ├── platforms/**/*.vni.json
│  └── pages.json
│
└── package.json
└── vue.config.js
```

## 文件说明

- `pages/**/*.vni.json`：用于存放 `pages` 与 `subpackages` 节点信息中所有页面的配置。
- `platforms/**/*.vni.json`：旧的规范需要兼容，同上。
- `_vni/condition.json`：配置 [condition](https://uniapp.dcloud.io/collocation/pages?id=condition) 节点信息。
- `_vni/easycom.json`：配置 [easycom](https://uniapp.dcloud.io/collocation/pages?id=easycom) 节点信息。
- `_vni/global-style.json`：配置 [globalstyle](https://uniapp.dcloud.io/collocation/pages?id=globalstyle) 节点信息。
- `_vni/tab-bar.json`：配置 [tabBar](https://uniapp.dcloud.io/collocation/pages?id=tabbar) 节点信息。
- `_vni/extras.json`：配置其它节点信息，如 preloadRule 等。如果有新增的规范，上面的配置文件未包含的情况下，均在这个文件配置即可。
