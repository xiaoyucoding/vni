# 页面配置

## 基础配置

基础配置，参考 uni-app 的[页面文档](https://uniapp.dcloud.io/collocation/pages?id=pages)即可。

下面对 vni 扩展或优化的配置做说明。

## 首页

`#home`节点用于标记首页。

示例如下

```json {6}
{
  "path": "pages/tabBar/component/component",
  "style": {
    "navigationBarTitleText": "内置组件"
  },
  "#home": true
}
```

::: warning

- 如果配置多个`#home`页面，vni 只会识别第一个读取到的`#home`页面，并且这个顺序是不确定的。
  :::

## 选项卡页

`#tab`节点用于设置页面与选项卡相关的信息。

| 属性       | 类型   | 说明                                                                                                |
| :--------- | :----- | :-------------------------------------------------------------------------------------------------- |
| sort       | Number | 排序，用于底部选项卡的主动排序，0-9。                                                                    |
| ...options | Any    | [tabBar](https://uniapp.dcloud.io/collocation/pages?id=tabbar) 的`list`项，除`pagePath`之外的配置。 |

示例如下

```json {6}
{
  "path": "pages/tabBar/API/API",
  "style": {
    "navigationBarTitleText": "接口"
  },
  "#tab": {
    "iconPath": "static/api.png",
    "selectedIconPath": "static/apiHL.png",
    "text": "接口",
    "sort": 1
  }
}
```

## 分包页

`#subpackage`节点用于设置分包相关信息。

| 属性       | 类型   | 说明                                                                                                                         |
| :--------- | :----- | :--------------------------------------------------------------------------------------------------------------------------- |
| root       | String | 分包根目录                                                                                                                   |
| ...options | Any    | [subpackages](https://uniapp.dcloud.io/collocation/pages?id=subpackages) 子项中除了`root`、`pages`之外的属性，例如`name`等。 |

示例如下

```json {6}
{
  "path": "pages/API/action-sheet/action-sheet",
  "style": {
    "navigationBarTitleText": "操作菜单"
  },
  "#subpackage": {
    "root": "pages/API",
    "name": "api"
  }
}
```

## 条件编译

实现不同平台编译出不同页面的需求，uni-app 目前的方案是在`pages.json`中使用[条件编译](https://uniapp.dcloud.io/platform?id=%e6%9d%a1%e4%bb%b6%e7%bc%96%e8%af%91)来解决。

vni 对此做了扩展配置，使用标准的 JSON 格式。

| 属性    | 类型                            | 说明                                |
| :------ | :------------------------------ | :---------------------------------- |
| #ifdef  | Array\<PlatformName\> \| String | 对应`#ifdef`，即包含在哪些平台。    |
| #ifndef | Array\<PlatformName\> \| String | 对应`#ifndef`，即不包含在哪些平台。 |

**PlatformName**

在 vni 的配置中，平台信息均为小写。输入的时候比较方便，但最终都会转换为大写，即 uni-app 规范的值。

| vni 规范可取值 | 对应 uni-app 规范的值 |
| :------------- | :-------------------- |
| app-plus       | APP-PLUS              |
| h5             | H5                    |
| mp-weixin      | MP-WEIXIN             |
| mp-alipay      | MP-ALIPAY             |
| mp-baidu       | MP-BAIDU              |

::: tip

- 上面只列出了常用的平台信息并不代表全部，更多可取值参考[官方文档](https://uniapp.dcloud.net.cn/tutorial/platform.html#preprocessor)。

  :::

示例如下

```json {6}
{
  "path": "pages/about/about",
  "style": {
    "navigationBarTitleText": "关于"
  },
  "#ifdef": ["h5", "app-plus"]
}
```

```json {6}
{
  "path": "pages/component/video/video",
  "style": {
    "navigationBarTitleText": "video"
  },
  "#ifndef": "mp-alipay"
}
```
