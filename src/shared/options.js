export const ProjectModeEnum = {
  NOT_UNI: 0, // 不是 uni-app 项目
  HX: 1, // HBuilderX
  CLI: 2, // Vue CLI
}
// vni 的部分配置文件名
export const vniConfigFileEnum = {
  TAB_BAR: 'tab-bar',
  GLOBAL_STYLE: 'global-style',
  EASY_COM: 'easycom',
  CONDITION: 'condition',
  EXTRAS: 'extras',
}
