import { outputJsonSync, outputFileSync } from 'fs-extra'
import { resolveFilePath, VNI_PAGE_SUFFIX, logSuccess } from '@/shared'

const pagePathPattern = /^[a-z\d\-\/]+$/
const pageNamePattern = /\/?([a-z\d\-]+)$/

const PAGE_TEMPLATE = `<template>
  <view></view>
</template>

<script>
  export default {}
</script>

<style>
</style>
`

export function generateNewPagePath(path) {
  if (!pagePathPattern.test(path)) {
    throw Error('页面路径格式不正确')
  }
  // 处理下传入的 path
  const matches = path.match(pageNamePattern)
  const pageName = matches[1]
  // 按照 pages/demo/demo.vue 的格式创建文件，即最后一级是目录，目录下有同名的文件。
  let pagePath = `${path}/${pageName}`
  if (pagePath.indexOf('pages') !== 0) {
    pagePath = `pages/${pagePath}`
  }
  return pagePath
}

export function createPage(options) {
  logSuccess(`开始创建新页面`)
  const pagePath = generateNewPagePath(options.path)
  // 输出文件
  outputFileSync(resolveFilePath(`${pagePath}.vue`), PAGE_TEMPLATE)
  outputJsonSync(
    resolveFilePath(`${pagePath}${VNI_PAGE_SUFFIX}.json`),
    {
      path: pagePath,
    },
    { spaces: 4 }
  )
  logSuccess(`新页面 ${pagePath} 创建成功`)
}
