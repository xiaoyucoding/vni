import { pathExistsSync } from 'fs-extra'
import { ProjectModeEnum } from './options'
import { VNI_CONFIG_PATH, VNI_PAGE_SUFFIX } from './constants'
import { join } from 'path'

export const PROJECT_MODE = (() => {
  // 去找 manifest.json 在哪
  const isHBuilderX = pathExistsSync('manifest.json')
  if (isHBuilderX) {
    return ProjectModeEnum.HX
  } else {
    return pathExistsSync('src/manifest.json') ? ProjectModeEnum.CLI : ProjectModeEnum.NOT_UNI
  }
})()

export function resolveFilePath(...rest) {
  return join(FILE_ROOT_PATH, ...rest).replace(/\\/g, '/')
}

export const FILE_ROOT_PATH = PROJECT_MODE === ProjectModeEnum.CLI ? 'src/' : ''

// pages.json 文件的路径
export const UNI_PAGES_JSON_PATH = resolveFilePath('pages.json')
// vni 配置文件的 glob 规则
export const VNI_CONFIG_PATH_PATTERN = `${FILE_ROOT_PATH}${VNI_CONFIG_PATH}/*.json`
// vni 规范的页面配置文件
export const VNI_PAGE_CONFIG_PATTERN = `${FILE_ROOT_PATH}*(pages|platforms)/**/*${VNI_PAGE_SUFFIX}.json`
