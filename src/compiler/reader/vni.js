import { vniConfigFileEnum, VNI_CONFIG_PATH_PATTERN, VNI_PAGE_CONFIG_PATTERN } from '@/shared'
import { readJsonSync } from 'fs-extra'
import { camelCase } from 'lodash'
const glob = require('glob')
// 读取 vni 配置
export function readVniFiles() {
  const routes = readVniRouteFiles()
  const profile = readVniProfile()
  return {
    routes,
    profile,
  }
}
// 读取页面配置文件
export function readVniRouteFiles() {
  const routeFilePathList = glob.sync(VNI_PAGE_CONFIG_PATTERN)
  const routes = []
  routeFilePathList.forEach((routeFilePath) => {
    routes.push(readJsonSync(routeFilePath))
  })
  return routes
}
// 读取配置目录下的所有文件
export function readVniProfile() {
  const filePathList = glob.sync(VNI_CONFIG_PATH_PATTERN)
  const profile = {}
  let extras = {}
  filePathList.forEach((filePath) => {
    const propName = camelCase(filePath.match(/([a-z\-]+)\.json/)[1])
    if (propName !== vniConfigFileEnum.EXTRAS) {
      profile[propName] = readJsonSync(filePath)
    } else {
      extras = readJsonSync(filePath)
    }
  })
  return {
    ...profile,
    ...extras,
  }
}
