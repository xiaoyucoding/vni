# 介绍

## vni 是什么

vni 是一个用于增强 [uni-app](https://uniapp.dcloud.io/) 开发体验，外置的工具。

## vni 能做什么

原生微信小程序（以下简称 mina）与 uni-app 项目结构，存在一处较为明显的差异。

- mina 每个页面都有一个 [${page}.json](https://developers.weixin.qq.com/miniprogram/dev/reference/configuration/page.html) 文件。
- uni-app 的所有页面配置都在 [pages.json](https://uniapp.dcloud.io/collocation/pages) 这一个公共的文件中。

uni-app 的这种设计意味着，每当有页面的配置需要改变，就要去操作 `pages.json` 这个文件。在多人协作开发时，这样做可能使开发人员需要经常应对 `pages.json` 文件的冲突问题。

对于这一点，vni 给出的解决思路如下：

- 采用 mina 的单个页面配置，即每个 `${page}.vue` 文件同级目录下有一个同名的 `${page}.vni.json` 文件。
- 为一些常用的配置也提供单独的文件，例如 globalStyle 等。
- 最后根据所有的相关文件，生成 uni-app 规范的 pages.json 文件。

注意，页面目录下生成的是 `${page}.vni.json`，明确标记这是一个 vni 的配置文件。

## 实践经验

- .gitignore 中忽略掉原始的 `pages.json` 文件
- .gitignore 中忽略掉 `_vni/condition.json` 文件
- 打包项目时，先执行一下 vni 的 build 命令，确保生成最新的 `pages.json` 文件。
