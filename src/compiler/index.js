import { readUniPagesJsonFile, readVniFiles } from './reader'
import { parseUniPagesJsonToVni, parseVniConfigToUniPagesJsonString } from './parser'
import { writeVniFiles, writeUniPagesJson } from './writer'
// 根据 uni-app 的 pages.json 文件生成 vni 规范的配置文件
export function translateUniPagesJsonToVniFiles() {
  // 读取 pages.json 文件
  const pagesJsonString = readUniPagesJsonFile()
  const vniOptions = parseUniPagesJsonToVni(pagesJsonString)
  // 生成文件
  return writeVniFiles(vniOptions)
}
// 根据 vni 规范的配置文件生成 uni-app 的 pages.json 文件
export function translateVniFilesToUniPagesJson() {
  const vniData = readVniFiles()
  const pagesJsonString = parseVniConfigToUniPagesJsonString(vniData)
  // 生成文件
  return writeUniPagesJson(pagesJsonString)
}
