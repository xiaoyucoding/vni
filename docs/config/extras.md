# 其它配置

以下配置文件与标准的 uni-app 配置完全一致：

- condition.json
- easycom.json
- global-style.json

下面重点说明存在差异的配置文件。

## tab-bar.json

`tab-bar.json` 对应 `pages.json` 的 `tabBar` 节点，区别在于不需要 `list` 属性，而是根据页面的 `#tab` 属性来识别。

```json
{
  "color": "#7A7E83",
  "selectedColor": "#007AFF",
  "borderStyle": "black",
  "backgroundColor": "#F8F8F8"
}
```

`list` 属性只需填写页面的路径信息即可，其它信息会在页面[配置](./page.md#选项卡页)的指定节点中设置。

## extras.json

用于填写除了核心的配置外，`pages.json` 中的其它信息：

- `preloadRule`
- `workers`
- 后续可能新增的配置项

```json
{
  "workers": "workers"
}
```
