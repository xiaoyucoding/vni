// 在所有页面中匹配选项卡页
export function findTabBarPageIndex(routes, tab) {
  const index = routes.findIndex((route) => {
    return route.path === tab.pagePath
  })
  return index
}
