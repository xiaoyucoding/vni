import { readFileSync } from 'fs-extra'
import { UNI_PAGES_JSON_PATH } from '@/shared'
// 读取 uni-app 项目的 pages.json 文件
export function readUniPagesJsonFile() {
  return readFileSync(UNI_PAGES_JSON_PATH, 'utf8')
}
