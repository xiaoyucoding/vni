const path = require('path')
const alias = require('@rollup/plugin-alias')
const nodeResolve = require('@rollup/plugin-node-resolve').nodeResolve
const commonjs = require('@rollup/plugin-commonjs')
const babel = require('@rollup/plugin-babel').babel
const terser = require('rollup-plugin-terser').terser

const plugins = [
  alias({
    entries: [
      {
        find: '@',
        replacement: path.resolve(__dirname, 'src'),
      },
    ],
  }),
  nodeResolve(),
  commonjs(),
  babel({
    babelHelpers: 'bundled',
  }),
]

if (process.env.NODE_ENV === 'production') {
  plugins.push(terser())
}

module.exports = {
  input: 'src/index.js',
  output: {
    file: 'dist/index.js',
    format: 'cjs',
  },
  plugins,
}
