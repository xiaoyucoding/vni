import { SUBPACKAGES_TAG, cloneObjectByFilterKeys } from '@/shared'
import { patchUniPlatformToPageList } from '../platform'

// 处理分包页的配置
export function patchVniSubpackagePage(page, options) {
  // 页面路径补全
  const pagePath = mergeSubpackagePagePath(page.path, options.root)

  return {
    ...page,
    path: pagePath, // 这里直接覆盖原来的 path
    [SUBPACKAGES_TAG]: options,
  }
}

function normalizeSubpackagePage(page) {
  const subPage = cloneObjectByFilterKeys(page, [SUBPACKAGES_TAG])
  subPage.path = splitSubpackagePagePath(page.path, page[SUBPACKAGES_TAG].root)
  return subPage
}

// 合并分包项
export function groupSubpackagePageList(subpackagePageList) {
  const subpackages = []
  subpackagePageList.forEach((subpackagePage) => {
    const subIndex = subpackages.findIndex((item) => {
      return item.root === subpackagePage[SUBPACKAGES_TAG].root
    })
    const subPage = normalizeSubpackagePage(subpackagePage)
    if (subIndex > -1) {
      subpackages[subIndex].pages.push(subPage)
    } else {
      subpackages.push({
        ...subpackagePage[SUBPACKAGES_TAG],
        pages: [subPage],
      })
    }
  })
  subpackages.forEach((subpackage) => {
    subpackage.pages = patchUniPlatformToPageList(subpackage.pages)
  })
  return subpackages
}

// 拼接分包页面的路径
export function mergeSubpackagePagePath(path, root) {
  const pagePath = root ? `${root}/${path}` : path
  // 如果出现连续的 / 则替换成一个
  return pagePath.replace(/\/{2,3}/, '/')
}
// 拆分分包页面的路径
export function splitSubpackagePagePath(path, root) {
  path = path.replace(root, '')
  // 移除开头的 / 符号
  return path.replace(/^\//, '')
}
