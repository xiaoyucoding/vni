import { has } from 'lodash'
import { patchVniSubpackagePage } from './subpackages'
import { isUniPlatformStart, isUniPlatformEnd, patchVniPagePlatform, patchUniPlatformToPageList } from '../platform'
import { HOME_PAGE_TAG, TAB_BAR_PAGE_TAG, SUBPACKAGES_TAG } from '@/shared'
import { parseVniTabBarPageToUni } from '../tab-bar'

// 遍历页面集合同时处理条件编译
export function patchPageList(pages, options = {}) {
  const list = []
  // 条件编译
  let platform = null

  pages.forEach((page) => {
    // 页面配置
    if (has(page, 'path')) {
      // 分包页面
      if (has(options, 'root')) {
        page = patchVniSubpackagePage(page, options)
      }
      if (platform) {
        page = patchVniPagePlatform(page, platform)
      }

      list.push(page)
    } else if (isUniPlatformStart(page)) {
      platform = page
    } else if (isUniPlatformEnd(page)) {
      platform = null
    }
  })

  return list
}
// 将不同类型的页面进行分组等
export function groupPageList(routes) {
  const mainPageList = []
  const subpackagePageList = []
  const tabBarPageList = []

  routes.forEach((route) => {
    if (has(route, SUBPACKAGES_TAG)) {
      subpackagePageList.push(route)
    } else if (has(route, TAB_BAR_PAGE_TAG)) {
      tabBarPageList.push(parseVniTabBarPageToUni(route))

      delete route[TAB_BAR_PAGE_TAG]
      // 如果有选项卡页，首页可能也在这里。
      if (has(route, HOME_PAGE_TAG)) {
        delete route[HOME_PAGE_TAG]
        mainPageList.unshift(route)
      } else {
        mainPageList.push(route)
      }
    } else if (has(route, HOME_PAGE_TAG)) {
      delete route[HOME_PAGE_TAG]
      mainPageList.unshift(route)
    } else {
      mainPageList.push(route)
    }
  })
  // 需要主动排序
  const sortedTabBarPageList = tabBarPageList.sort((cur, next) => {
    return cur.sort - next.sort
  })

  return {
    mainPageList: patchUniPlatformToPageList(mainPageList),
    tabBarPageList: sortedTabBarPageList,
    subpackagePageList,
  }
}
