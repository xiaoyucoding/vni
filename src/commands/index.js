import { translateUniPagesJsonToVniFiles, translateVniFilesToUniPagesJson } from '@/compiler'
import { pathExistsSync, removeSync } from 'fs-extra'
import {
  resolveFilePath,
  VNI_CONFIG_PATH,
  VNI_CONFIG_PATH_PATTERN,
  VNI_PAGE_CONFIG_PATTERN,
  logSuccess,
  logInfo,
  logError,
} from '@/shared'

const glob = require('glob')
const chokidar = require('chokidar')

// 初始化配置文件
export function init() {
  logInfo('init start')
  translateUniPagesJsonToVniFiles()
  logSuccess('init finished')
}
// 清空配置
export function clear() {
  logInfo('clear start...')
  // 先移除目录
  const vniConfigDir = resolveFilePath(VNI_CONFIG_PATH)
  logInfo(`remove ${vniConfigDir}`)
  removeSync(vniConfigDir)
  // 再移除页面文件
  const pageFilePaths = glob.sync(VNI_PAGE_CONFIG_PATTERN)
  pageFilePaths.forEach((filePath) => {
    logInfo(`remove ${filePath}`)
    removeSync(filePath)
  })
  logSuccess('clear finished')
}
// 构建 pages.json
export function build() {
  logInfo('build start')
  translateVniFilesToUniPagesJson()
  logSuccess('build finished')
}
// 开发模式
export function dev() {
  // 监听文件变化
  // 先检测下是否有目录
  const hasVniConfig = pathExistsSync(resolveFilePath(VNI_CONFIG_PATH))
  if (hasVniConfig) {
    const watcher = chokidar.watch([VNI_CONFIG_PATH_PATTERN, VNI_PAGE_CONFIG_PATTERN])
    let isReady = false
    watcher
      .on('ready', () => {
        logSuccess('watcher ready')
        isReady = true
        build()
      })
      .on('add', (path) => {
        if (isReady) {
          logInfo(`add file ${path}`)
          build()
        }
      })
      .on('change', (path) => {
        if (isReady) {
          logInfo(`change file ${path}`)
          build()
        }
      })
      .on('unlink', (path) => {
        logInfo(`unlink file ${path}`)
        if (isReady) {
          build()
        }
      })
  } else {
    // 日志提示下
    logError(`未找到 vni 配置目录`)
  }
}
