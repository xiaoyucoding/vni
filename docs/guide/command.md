# 命令

vni 定义了一些命令，它们需要在 uni-app 项目根目录下执行。

- HBuilderX 创建的，在 manifest.json 文件同级执行。
- Vue CLI 创建的，则在 vue.config.js 文件同级执行。

## init

```bash
$ vni init
```

根据 uni-app 项目的 `pages.json` 生成 vni 规范的的配置文件，详见[目录结构](./directory-structure.md)中的说明。

### 注释

vni 初始化操作，对 uni-app 的 `pages.json` 中注释的支持情况如下。

- 提示首页的注释，支持。

```json {2}
{
  "pages": [
    //pages数组中第一项表示应用启动页，参考：https://uniapp.dcloud.io/collocation/pages
    {
      "path": "pages/index/index",
      "style": {
        "navigationBarTitleText": "uni-app"
      }
    }
  ],
  "globalStyle": {
    "navigationBarTextStyle": "black",
    "navigationBarTitleText": "uni-app",
    "navigationBarBackgroundColor": "#F8F8F8",
    "backgroundColor": "#F8F8F8"
  }
}
```

- 条件编译注释，支持。

```json {2,9,16}
{
  "pages": [
    //pages数组中第一项表示应用启动页，参考：https://uniapp.dcloud.io/collocation/pages
    {
      "path": "pages/index/index",
      "style": {
        "navigationBarTitleText": "uni-app"
      }
    },
    ,
    // #ifdef H5
    {
      "path": "pages/about/about",
      "style": {
        "navigationBarTitleText": "关于"
      }
    }
    // #endif
  ],
  "globalStyle": {}
}
```

- 其它普通注释的情况，支持。

```json {11}
{
  "pages": [
    {
      "path": "pages/index/index",
      "style": {
        "navigationBarTitleText": "uni-app"
      }
    },
    ,
    // #ifdef H5
    {
      "path": "pages/about/about", //这个页面只在 H5 平台显示
      "style": {
        "navigationBarTitleText": "关于"
      }
    }
    // #endif
  ],
  "globalStyle": {}
}
```

::: danger

- 单个配置项的条件编译，vni 是不支持的。
  :::

```json {11,13}
{
  "pages": [
    {
      "path": "pages/index/index",
      "style": {
        "navigationBarTitleText": "uni-app"
      }
    }
  ],
  "globalStyle": {
    // #ifdef MP-360
    "navigationStyle": "custom"
    // #endif
  }
}
```

这种情况，需要使用 uni-app 提供的平台配置项，而非条件编译。

```json {11}
{
  "pages": [
    {
      "path": "pages/index/index",
      "style": {
        "navigationBarTitleText": "uni-app"
      }
    }
  ],
  "globalStyle": {
    "mp-360": {
      "navigationStyle": "custom"
    }
  }
}
```

## build

```bash
$ vni build
```

根据配置文件的数据，合并生成 uni-app 规范的 `pages.json` 文件。

## dev

```bash
$ vni dev
```

dev 就是 build 的监听模式，调用后会立即执行一次 build 操作。当以下文件变化时，执行 build 操作。

- `pages/**/*.vni.json`
- `platforms/**/*.vni.json`
- `_vni/*.json`

**Tip**

- 目前监听了`add`、`change`、`unlink`事件，可能需要继续完善。

## clear

```bash
$ vni clear
```

移除 `_vni/` 目录，以及所有 `${page}.vni.json` 文件。
