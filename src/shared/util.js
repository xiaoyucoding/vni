// 复制一个对象并且过滤掉指定的属性
export function cloneObjectByFilterKeys(source, keys) {
  const target = {}
  const sourceKeys = Object.keys(source).filter((key) => {
    return keys.indexOf(key) === -1
  })
  sourceKeys.forEach((key) => {
    target[key] = source[key]
  })
  return target
}

function _formatDateText(value) {
  return value > 9 ? value : '0' + value
}

export function formatDateTime(date) {
  date = date || new Date()
  const year = date.getFullYear()
  const month = _formatDateText(date.getMonth() + 1)
  const day = _formatDateText(date.getDate())

  const hours = _formatDateText(date.getHours())
  const minutes = _formatDateText(date.getMinutes())
  const seconds = _formatDateText(date.getSeconds())

  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
}
