// 配置文件的目录
export const VNI_CONFIG_PATH = '_vni'
// #start 条件编译的占位符
export const INCLUDE_TAG = '#ifdef'
export const EXCLUDE_TAG = '#ifndef'
export const END_TAG = '#endif'
export const VNI_PLATFORM_TAG = '#platform'
export const SPLIT_SIGN = '||'
// #end
// 分包
export const SUBPACKAGES_TAG = '#subpackage'
// 首页
export const HOME_PAGE_TAG = '#home'
// 选项卡页
export const TAB_BAR_PAGE_TAG = '#tab'

// 页面配置后缀
export const VNI_PAGE_SUFFIX = '.vni'
