import { outputFileSync } from 'fs-extra'
import { UNI_PAGES_JSON_PATH } from '@/shared'

export function writeUniPagesJson(pagesJsonString) {
  outputFileSync(UNI_PAGES_JSON_PATH, pagesJsonString, 'utf8')
}
