import strip from 'strip-comments'
import { INCLUDE_TAG, EXCLUDE_TAG, END_TAG } from '@/shared'

// 移除多余的注释以及标记条件编译时产生的多余符号
function stripUniPagesJsonComments(pagesJsonString) {
  pagesJsonString = strip(pagesJsonString)
    .replace(/\s+/g, '')
    .replace(/,,/g, ',')
    .replace(/\[,/g, '[')
    .replace(/,\]/g, ']')
  // 主要处理 buttons 中的 text 字符，'\ue123' 应当转换为 '\\ue123'，否则会被转义进而丢失字符串信息。
  pagesJsonString = pagesJsonString.replace(/"(\\u[a-z0-9]+)/g, `"\\$1`)
  // 忽略大小写，兼容 subpackages 与 subPackages
  pagesJsonString = pagesJsonString.replace(/"subpackages"/i, `"subpackages"`)

  return pagesJsonString
}

// 条件编译
const includePattern = /\/\/(?:\s+)?#ifdef([A-Z\d\-\|\s]+)/g
const excludePattern = /\/\/(?:\s+)?#ifndef([A-Z\d\-\|\s]+)/g
const endPattern = /\/\/(?:\s+)?#endif/g
// 这个是哪种场景？
// const endifProp = /"(,)?(?:[\s\t\n]+)?\/\/(?:\s+)?#endif/g

// 将 uni 的条件编译注释格式标记为 vni 的配置项
export function parseUniPlatformToVni(pagesJsonString) {
  // include
  pagesJsonString = pagesJsonString.replace(includePattern, (matches, $1) => {
    return `,{"${INCLUDE_TAG}": "${$1.replace(/\s+/g, '')}"},`
  })
  // exclude
  pagesJsonString = pagesJsonString.replace(excludePattern, (matches, $1) => {
    return `,{"${EXCLUDE_TAG}": "${$1.replace(/\s+/g, '')}"},`
  })
  // end
  pagesJsonString = pagesJsonString.replace(endPattern, `,{"${END_TAG}": true},`)

  return stripUniPagesJsonComments(pagesJsonString)
}

// vni 的条件编译占位标签
const vniPlatformTagPattern = /(,?[\n\s]+){[\n\s]+"#platform":\s+"([a-zA-Z\d\-\#\/\|\s]+)"[\n\s]+},?/g
// 产生的多余逗号
const invalidSignPattern = /}(,[\n\s]+\/\/\s+#endif[\n\s]+)\]/g

export function parseVniPlatformToUni(pagesJsonString) {
  pagesJsonString = pagesJsonString.replace(vniPlatformTagPattern, '$1$2')
  pagesJsonString = pagesJsonString.replace(invalidSignPattern, (matches, $1) => {
    return `}${$1.replace(',', '')}]`
  })

  return pagesJsonString
}
